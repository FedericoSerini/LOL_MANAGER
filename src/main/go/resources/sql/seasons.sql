create table seasons (
  id int(20) primary key,
  name varchar (75)
);

insert into seasons (id, name) values (0,"PRESEASON 3");
insert into seasons (id, name) values (1,"SEASON 3");
insert into seasons (id, name) values (2,"PRESEASON 2014");
insert into seasons (id, name) values (3,"SEASON 2014");
insert into seasons (id, name) values (4,"PRESEASON 2015");
insert into seasons (id, name) values (5,"SEASON 2015");
insert into seasons (id, name) values (6,"PRESEASON 2016");
insert into seasons (id, name) values (7,"SEASON 2016");
insert into seasons (id, name) values (8,"PRESEASON 2017");
insert into seasons (id, name) values (9,"SEASON 2017");
insert into seasons (id, name) values (10,"PRESEASON 2018");
insert into seasons (id, name) values (11,"SEASON 2018");
insert into seasons (id, name) values (12,"PRESEASON 2019");
insert into seasons (id, name) values (13,"SEASON 2019");
insert into seasons (id, name) values (14,"PRESEASON 2020");
insert into seasons (id, name) values (15,"SEASON 2020");
