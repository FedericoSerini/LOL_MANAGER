create table game_modes
(
  id                int(20) primary key,
  name              varchar(255),
  description       varchar(500)
);

insert into game_modes (id,name, description) values
  (1,"CLASSIC", "Classic Summoner's Rift and Twisted Treeline games");
insert into game_modes (id,name, description) values
  (2,"ODIN", "Dominion/Crystal Scar games");
insert into game_modes (id,name, description) values
  (3,"ARAM", "ARAM games");
insert into game_modes (id,name, description) values
  (4,"TUTORIAL", "Tutorial games");
insert into game_modes (id,name, description) values
  (5,"URF", "URF games");
insert into game_modes (id,name, description) values
  (6,"DOOMBOTSTEEMO", "Doom Bot games");
insert into game_modes (id,name, description) values
  (7,"ONEFORALL", "One for All games");
insert into game_modes (id,name, description) values
  (8,"ASCENSION", "Ascension games");
insert into game_modes (id,name, description) values
  (9,"FIRSTBLOOD", "Snowdown Showdown games");
insert into game_modes (id,name, description) values
  (10,"KINGPORO", "Legend of the Poro King games");
insert into game_modes (id,name, description) values
  (11,"SIEGE", "Nexus Siege games");
insert into game_modes (id,name, description) values
  (12,"ASSASSINATE", "Blood Hunt Assassin games");
insert into game_modes (id,name, description) values
  (13,"ARSR", "	All Random Summoner's Rift games");
insert into game_modes (id,name, description) values
  (14,"DARKSTAR", "Dark Star: Singularity games");
insert into game_modes (id,name, description) values
  (15,"STARGUARDIAN", "Star Guardian Invasion games");
insert into game_modes (id,name, description) values
  (16,"PROJECT", "PROJECT: Hunters games");











