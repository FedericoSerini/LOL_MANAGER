CREATE TABLE champions_mastery
(
    id integer PRIMARY KEY AUTOINCREMENT,
	  champion_level 			int,
	  chest_granted 	boolean,
	  champion_points int,
	  champion_points_since_last_level int,
	  player_id int, --FK
	  champion_points_until_next_level int,
	  tokens_earned int,
	  champion_id int, --FK
	  last_play_time int(20),
	  CONSTRAINT champions_fk FOREIGN KEY (champion_id) REFERENCES champions (id),
    CONSTRAINT summoner_fk FOREIGN KEY (player_id) REFERENCES summoner (id)
);