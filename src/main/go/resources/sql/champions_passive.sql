CREATE TABLE champions_passive
(
    id integer PRIMARY KEY AUTOINCREMENT,
	  name 			varchar(255),
	  sanitized_description 	varchar(255),
		full_image varchar(255),
		group_image varchar(255),
		image_sprite varchar(255),
		image_height int,
		image_width int,
		image_y int,
		image_x int,
		description varchar(255)
);