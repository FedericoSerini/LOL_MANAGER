INSERT INTO mini_series VALUES (0, 0, 0, 0, "-- NO PROMO --")

CREATE TABLE mini_series
(
    id integer PRIMARY KEY AUTOINCREMENT,
  	wins		int,
	  losses		int,
	  target		int ,
	  progress	varchar(255)
);