create table match_reference
(
  id                int(20) primary key,
  lane              varchar(50),
  champion_id        int, -- FK
  platform_id       varchar (25),
  match_timestamp   int(20),
  queue             int,
  role              varchar(100),
  season            int,
  CONSTRAINT season_fk FOREIGN KEY (season) REFERENCES seasons (id)
);

