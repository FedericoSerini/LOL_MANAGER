CREATE TABLE summoner_league
(
    id integer PRIMARY KEY AUTOINCREMENT ,
  	rank				varchar(255),
	  queue_type			varchar(255),
	  hot_streak			boolean,
	  mini_series_id integer ,
	  wins				int,
	  veteran				boolean,
	  losses				int,
	  fresh_blood			boolean,
  	league_id			varchar(255),
	  player_or_team_name	varchar(255), -- fk
	  inactive			boolean,
	  player_or_team_id		varchar(255), -- fk
	  league_name			varchar(255),
	  tier				varchar(255),
	  league_points		int,
    CONSTRAINT promo_id_fk FOREIGN KEY (mini_series_id) REFERENCES mini_series (id)
)



