	CREATE TABLE champions_stats
(
    id integer PRIMARY KEY AUTOINCREMENT,
	  armor_per_level			float,
	  attack_damage 			float,
	  mana_points_per_level  	float,
	  attack_speed_offset   	float,
	  mana_points				float,
	  armor 					float,
	  health_points			float,
	  health_regen_per_level		float,
	  attack_speed_per_level 	float,
	  attack_range 			float,
	  movement_speed 			float,
  	attack_damage_per_level 	float,
	  mana_regen_per_level  		float,
  	critical_damage_per_level 	float,
	  magic_resist_per_level 	float,
	  critical_damage			float,
	  mana_regen				float,
	  magic_resist 			float,
	  health_regen				float,
	  health_points_per_level 	float
);
