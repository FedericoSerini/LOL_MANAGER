CREATE TABLE summoner
(
    id int PRIMARY KEY,
    account_id int,
    revision_date int,
    summoner_level int,
    name varchar(80),
    profile_icon_id int
);