CREATE TABLE items
(
    id int PRIMARY KEY,
    name varchar(500),
    description varchar(500),
    sanitized_description varchar(500),
    base_gold_price int,
    sell_gold_price int,
    total_full_item_gold int,
    item_stat_id integer,
    CONSTRAINT item_stats_fk FOREIGN KEY (item_stat_id) REFERENCES item_stats (id)
);