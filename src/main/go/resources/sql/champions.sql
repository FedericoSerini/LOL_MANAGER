
CREATE TABLE champions
(
    id int PRIMARY KEY,
    stat_id int, --FK
    name varchar(500),
    title varchar(500),
    passive_id integer , --FK
    spell_id integer, --FK
    mastery_id integer,
    CONSTRAINT champions_spells_fk FOREIGN KEY (spell_id) REFERENCES champions_spells (id),
    CONSTRAINT champions_stats_fk FOREIGN KEY (stat_id) REFERENCES champions_stats (id),
    CONSTRAINT champions_passive_fk FOREIGN KEY (passive_id) REFERENCES champions_passive (id),
    CONSTRAINT champions_mastery_fk FOREIGN KEY (mastery_id) REFERENCES champions_mastery (id)
)

