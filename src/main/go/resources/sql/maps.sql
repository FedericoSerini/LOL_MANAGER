create table maps
(
  id                int(20) primary key,
  name              varchar(255),
  game_mode         int,
  CONSTRAINT game_mode_fk FOREIGN KEY (game_mode) REFERENCES game_modes (id)
);
