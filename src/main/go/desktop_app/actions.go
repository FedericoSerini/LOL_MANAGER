package main

import (
	"log"
)

import (
	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"
)

var isSpecialMode = walk.NewMutableCondition()

type MyMainWindow struct {
	*walk.MainWindow
}

func main() {
	MustRegisterCondition("isSpecialMode", isSpecialMode)

	mw := new(MyMainWindow)

	var openAction, showAboutBoxAction *walk.Action
	var recentMenu *walk.Menu
	var toggleSpecialModePB *walk.PushButton

	if err := (MainWindow{
		AssignTo: &mw.MainWindow,
		Title:    "Championify4Go",
		MenuItems: []MenuItem{
			Menu{
				Text: "&File",
				Items: []MenuItem{
					Action{
						AssignTo:    &openAction,
						Text:        "&Open",
						Image:       "src/main/go/desktop_app/img/open.png",
						Enabled:     Bind("enabledCB.Checked"),
						Visible:     Bind("!openHiddenCB.Checked"),
						Shortcut:    Shortcut{walk.ModControl, walk.KeyO},
						OnTriggered: mw.openActionTriggered,
					},
					Menu{
						AssignTo: &recentMenu,
						Text:     "Recent",
					},
					Separator{},
					Action{
						Text:        "E&xit",
						OnTriggered: func() { mw.Close() },
					},
				},
			},
			Menu{
				Text: "&Help",
				Items: []MenuItem{
					Action{
						AssignTo:    &showAboutBoxAction,
						Text:        "About",
						OnTriggered: mw.showAboutBoxActionTriggered,
					},
				},
			},
		},
		ToolBar: ToolBar{
			ButtonStyle: ToolBarButtonImageBeforeText,
			Items: []MenuItem{
				ActionRef{&openAction},
				Menu{
					Text:  "New A",
					Image: "src/main/go/desktop_app/img/document-new.png",
					Items: []MenuItem{
						Action{
							Text:        "A",
							OnTriggered: mw.newActionTriggered,
						},
						Action{
							Text:        "B",
							OnTriggered: mw.newActionTriggered,
						},
						Action{
							Text:        "C",
							OnTriggered: mw.newActionTriggered,
						},
					},
					OnTriggered: mw.newActionTriggered,
				},
				Separator{},
				Menu{
					Text:  "View",
					Image: "src/main/go/desktop_app/img/document-properties.png",
					Items: []MenuItem{
						Action{
							Text:        "X",
							OnTriggered: mw.changeViewActionTriggered,
						},
						Action{
							Text:        "Y",
							OnTriggered: mw.changeViewActionTriggered,
						},
						Action{
							Text:        "Z",
							OnTriggered: mw.changeViewActionTriggered,
						},
					},
				},
				Separator{},
				Action{
					Text:        "Special",
					Image:       "src/main/go/desktop_app/img/system-shutdown.png",
					Enabled:     Bind("isSpecialMode && enabledCB.Checked"),
					OnTriggered: mw.specialActionTriggered,
				},
			},
		},
		ContextMenuItems: []MenuItem{
			ActionRef{&showAboutBoxAction},
		},
		MinSize: Size{400, 600},
		Layout:  VBox{},
		Children: []Widget{
			HSplitter{
				MinSize: Size{20, 20},
				MaxSize: Size{20, 20},
				Children: []Widget{
					TextEdit{
						ReadOnly: true,
					},
					PushButton{
						Name: "browseTB",
						Text: "Browse",
					},
				},
			},

			CheckBox{
				Name:    "enabledCB",
				Text:    "Open / Special Enabled",
				Checked: true,
			},
			CheckBox{
				Name:    "openHiddenCB",
				Text:    "Open Hidden",
				Checked: true,
			},
			PushButton{
				AssignTo: &toggleSpecialModePB,
				Text:     "Enable Special Mode",
				OnClicked: func() {
					isSpecialMode.SetSatisfied(!isSpecialMode.Satisfied())

					if isSpecialMode.Satisfied() {
						toggleSpecialModePB.SetText("Disable Special Mode")
					} else {
						toggleSpecialModePB.SetText("Enable Special Mode")
					}
				},
			},

			VSplitter{
				Children: []Widget{
					CheckBox{ //1
						Name:    "openHiddenCB",
						Text:    "Open Hidden",
						Checked: true,
					},
					CheckBox{ //2
						Name:    "splitUpElementsCB",
						Text:    "Split Up Item Sets",
						Checked: true,
					},
					CheckBox{ //3
						Name:       "shortHandSkillOrderCB",
						Text:       "Use shorthand for Skill Order (Q>W>E)",
						Checked:    true,
						Persistent: true,
					},
					CheckBox{ //4
						Name:    "enableConsumablesCB",
						Text:    "Enable Consumables",
						Checked: true,
					},
					CheckBox{ //5
						Name:    "enableTrinketsCB",
						Text:    "Enable Trinkets",
						Checked: true,
					},
					CheckBox{ //6
						Name:    "openHiddenCB",
						Text:    "Open Hidden",
						Checked: true,
					},
					CheckBox{ //7
						Name:    "openHiddenCB",
						Text:    "Open Hidden",
						Checked: true,
					},
				},
			},
			HSplitter{
				MinSize: Size{20, 20},
				MaxSize: Size{20, 20},
				Children: []Widget{
					PushButton{
						AssignTo: &toggleSpecialModePB,
						Text:     "IMPORT",
						OnClicked: func() {
							walk.MsgBox(mw, "IMPORTING", "IMPORTING!", walk.MsgBoxIconInformation)
						},
					},
					PushButton{
						AssignTo: &toggleSpecialModePB,
						Text:     "DELETE",
						OnClicked: func() {
							walk.MsgBox(mw, "DELETING", "DELETING!", walk.MsgBoxIconInformation)
						},
					},
				},
			},
		},
	}.Create()); err != nil {
		log.Fatal(err)
	}

	addRecentFileActions := func(texts ...string) {
		for _, text := range texts {
			a := walk.NewAction()
			a.SetText(text)
			a.Triggered().Attach(mw.openActionTriggered)
			recentMenu.Actions().Add(a)
		}
	}

	addRecentFileActions("Foo", "Bar", "Baz")

	mw.Run()
}

func (mw *MyMainWindow) openActionTriggered() {
	walk.MsgBox(mw, "Open", "Pretend to open rsrc.rc file...", walk.MsgBoxIconInformation)
}

func (mw *MyMainWindow) newActionTriggered() {
	walk.MsgBox(mw, "New", "Newing something up... or not.", walk.MsgBoxIconInformation)
}

func (mw *MyMainWindow) changeViewActionTriggered() {
	walk.MsgBox(mw, "Change View", "By now you may have guessed it. Nothing changed.", walk.MsgBoxIconInformation)
}

func (mw *MyMainWindow) showAboutBoxActionTriggered() {
	walk.MsgBox(mw, "About", "Walk Actions Example", walk.MsgBoxIconInformation)
}

func (mw *MyMainWindow) specialActionTriggered() {
	walk.MsgBox(mw, "Special", "Nothing to see here.", walk.MsgBoxIconInformation)
}
