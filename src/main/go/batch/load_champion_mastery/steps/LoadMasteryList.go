package steps

import (
	"../../../common/business_objects"
	databaseModel "../../../common/model"
	jsonModel "../../../common/model/json"
	"../../../common/utils"
	"database/sql"
	"encoding/json"
	"log"
)

func LoadMasteryData(connection *sql.DB) {
	response := utils.OpenFile("src/main/go/resources/json_samples/champion_mastery.json")

	var championMasteryTree jsonModel.ChampionMasteryTree
	var championMasteries []databaseModel.ChampionMastery

	log.Println("-- UNMARSHALLING CHAMPION MASTERY JSON --")
	err := json.Unmarshal(response, &championMasteryTree)

	if err != nil {
		log.Println(err)
	} else {
		log.Println("-- CHAMPION MASTERY JSON UNMASHALLED --")
	}

	for _, node := range championMasteryTree {
		championMastery := databaseModel.ChampionMastery{ChestGranted: *&node.ChestGranted, ChampionLevel: *&node.ChampionLevel,
			ChampionPoints: *&node.ChampionPoints, ChampionId: *&node.ChampionId, PlayerId: *&node.PlayerId,
			ChampionPointsUntilNextLevel: *&node.ChampionPointsUntilNextLevel, TokensEarned: *&node.TokensEarned, ChampionPointsSinceLastLevel: *&node.ChampionPointsSinceLastLevel,
			LastPlayTime: *&node.LastPlayTime}
		championMasteries = append(championMasteries, championMastery)
	}

	log.Println("-- CHECKING YOUR CHAMPION MASTERY DATA --")
	for _, element := range championMasteries {
		/* Check if the struct is empty */
		if (databaseModel.ChampionMastery{}) == business_objects.FindChampionMasteryByChampionId(connection, element.ChampionId) {
			log.Println("-- NO DATA, PERSISTING YOUR CHAMPION MASTERY DATA --")
			business_objects.PersistChampionMastery(connection, element)
		} else {
			log.Println("-- UPDATING YOUR CHAMPION MASTERY DATA --")
			business_objects.UpdateChampionMastery(connection, element)
		}
	}
}
