package main

import (
	"../../common/config"
	"./steps"
	"log"
	"time"
)

func main() {
	databaseConnection := config.GetDatabaseConnection()

	log.Println("------- LOADING CHAMPION MASTERY DATA STARTED @", time.Now(), "-------")
	steps.LoadMasteryData(databaseConnection)
	log.Println("------- LOADING CHAMPION MASTERY ENDED @", time.Now(), " -------")

	log.Println("CLOSING DATABASE CONNECTION")
	defer databaseConnection.Close()
	log.Println("DATABASE CONNECTION SUCCESSFULLY CLOSED")
}
