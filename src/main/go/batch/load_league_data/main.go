package main

import (
	"../../common/config"
	"./steps"
	"log"
	"time"
)

func main() {
	databaseConnection := config.GetDatabaseConnection()

	log.Println("------- LOADING LEAGUE DATA STARTED @", time.Now(), "-------")
	steps.LoadLeagueData(databaseConnection)
	log.Println("------- LOADING LEAGUE DATA ENDED @", time.Now(), " -------")

	log.Println("CLOSING DATABASE CONNECTION")
	defer databaseConnection.Close()
	log.Println("DATABASE CONNECTION SUCCESSFULLY CLOSED")
}
