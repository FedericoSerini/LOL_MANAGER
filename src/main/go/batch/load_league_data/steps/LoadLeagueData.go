package steps

import (
	"../../../common/business_objects"
	databaseModel "../../../common/model"
	jsonModel "../../../common/model/json"
	"../../../common/utils"
	"database/sql"
	"encoding/json"
	"log"
)

func LoadLeagueData(connection *sql.DB) {
	response := utils.OpenFile("src/main/go/resources/json_samples/league.json")

	var leagueTree jsonModel.LeagueTree
	var league databaseModel.League
	var miniSeriesId int32 = 0

	log.Println("-- UNMARSHALLING LEAGUE JSON --")
	err := json.Unmarshal(response, &leagueTree)

	if err != nil {
		log.Println(err)
	} else {
		log.Println("-- LEAGUE JSON UNMASHALLED --")
	}

	if *&leagueTree[0].MiniSeries != nil {
		miniSeriesId = 1 /* todo take last mini series id inserted  */
	}

	league = databaseModel.League{
		FreshBlood:       *&leagueTree[0].FreshBlood,
		HotStreak:        *&leagueTree[0].HotStreak,
		Inactive:         *&leagueTree[0].Inactive,
		LeagueId:         *&leagueTree[0].LeagueId,
		LeagueName:       *&leagueTree[0].LeagueName,
		LeaguePoints:     *&leagueTree[0].LeaguePoints,
		Losses:           *&leagueTree[0].Losses,
		PlayerOrTeamId:   *&leagueTree[0].PlayerOrTeamId,
		QueueType:        *&leagueTree[0].QueueType,
		PlayerOrTeamName: *&leagueTree[0].PlayerOrTeamName,
		Rank:             *&leagueTree[0].Rank,
		Tier:             *&leagueTree[0].Tier,
		Veteran:          *&leagueTree[0].Veteran,
		Wins:             *&leagueTree[0].Wins,
		MiniSeries:       miniSeriesId,
	}

	log.Println("-- CHECKING YOUR LEAGUE DATA --")

	/* Check if the struct is empty */
	if (databaseModel.League{}) == business_objects.FindLeagueByPlayerId(connection, league.PlayerOrTeamId) {
		log.Println("-- NO DATA, PERSISTING YOUR LEAGUE DATA --")
		business_objects.PersistLeague(connection, league)
	} else {
		log.Println("-- UPDATING YOUR LEAGUE DATA --")
		business_objects.UpdateLeague(connection, league)
	}

}
