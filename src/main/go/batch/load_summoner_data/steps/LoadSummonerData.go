package steps

import (
	"../../../common/business_objects"
	databaseModel "../../../common/model"
	jsonModel "../../../common/model/json"
	"../../../common/utils"
	"database/sql"
	"encoding/json"
	"log"
)

func LoadSummonerData(connection *sql.DB) {
	response := utils.OpenFile("src/main/go/resources/json_samples/summoner.json")
	var summoner jsonModel.Summoner
	var summonerData databaseModel.Summoner

	log.Println("-- UNMARSHALLING SUMMONER JSON --")
	err := json.Unmarshal(response, &summoner)

	if err != nil {
		log.Println(err)
	} else {
		log.Println("-- SUMMONER JSON UNMASHALLED --")
	}

	summonerData = databaseModel.Summoner{
		ProfileIconId: *&summoner.ProfileIconId,
		Name:          *&summoner.Name,
		SummonerLevel: *&summoner.SummonerLevel,
		RevisionDate:  *&summoner.RevisionDate,
		Id:            *&summoner.Id,
		AccountId:     *&summoner.AccountId,
	}

	log.Println("-- PERSISTING SUMMONER DATA --")
	business_objects.PersistSummoner(connection, summonerData)

}
