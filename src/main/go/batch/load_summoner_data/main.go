package main

import (
	"../../common/config"
	"./steps"
	"log"
	"time"
)

func main() {
	databaseConnection := config.GetDatabaseConnection()

	log.Println("------- LOADING SUMMONER DATA STARTED @", time.Now(), "-------")
	steps.LoadSummonerData(databaseConnection)
	log.Println("------- LOADING SUMMONER DATA ENDED @", time.Now(), " -------")

	log.Println("CLOSING DATABASE CONNECTION")
	defer databaseConnection.Close()
	log.Println("DATABASE CONNECTION SUCCESSFULLY CLOSED")
}
