package steps

import (
	databaseModel "../../../common/model"
	jsonModel "../../../common/model/json"
	"../../../common/utils"
	"database/sql"
	"encoding/json"
	"log"
)

func LoadLolStatus(connection *sql.DB) {
	/* response := http_utils.GetRequest("https://euw1.api.riotgames.com/lol/static-data/v3/items?" +
	"locale=en_US&itemListData=all&tags=all&") TODO MODIFY*/
	response := utils.OpenFile("src/main/go/resources/json_samples/lol_status.json")
	var statusListTree jsonModel.StatusesTree
	var statuses []databaseModel.Status

	log.Println("-- UNMARSHALLING LOL STATUS JSON --")
	err := json.Unmarshal(response, &statusListTree)

	if err != nil {
		log.Println(err)
	} else {
		log.Println("-- LOL STATUS JSON UNMASHALLED --")
	}

	for _, element := range statuses {
		log.Println("-- PERSISTING LOL STATUS --")
		log.Println(element)
		//business_objects.PersistMatchReference(connection,element)
	}
}
