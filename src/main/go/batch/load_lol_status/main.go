package main

import (
	"../../common/config"
	"./steps"
	"log"
	"time"
)

func main() {
	databaseConnection := config.GetDatabaseConnection()

	log.Println("------- LOADING LOL STATUS STARTED @", time.Now(), "-------")
	steps.LoadLolStatus(databaseConnection)
	log.Println("------- LOADING LOL STATUS ENDED @", time.Now(), " -------")

	log.Println("CLOSING DATABASE CONNECTION")
	defer databaseConnection.Close()
	log.Println("DATABASE CONNECTION SUCCESSFULLY CLOSED")
}
