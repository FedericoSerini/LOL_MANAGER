package steps

import (
	//"../../../common/http_utils"
	"../../../common/business_objects"
	databaseModel "../../../common/model"
	jsonModel "../../../common/model/json"
	"../../../common/utils"
	"database/sql"
	"encoding/json"
	"log"
)

func LoadMatchList(connection *sql.DB) {
	/* response := http_utils.GetRequest("https://euw1.api.riotgames.com/lol/static-data/v3/items?" +
	"locale=en_US&itemListData=all&tags=all&") */
	response := utils.OpenFile("src/main/go/resources/json_samples/match_list.json")
	var matchListTree jsonModel.MatchListTree
	var matchReferences []databaseModel.MatchReference

	log.Println("-- UNMARSHALLING MATCH LIST JSON --")
	err := json.Unmarshal(response, &matchListTree)

	if err != nil {
		log.Println(err)
	} else {
		log.Println("-- MATCH LIST JSON UNMASHALLED --")
	}

	for _, node := range matchListTree.Matches {
		matchReference := databaseModel.MatchReference{GameId: *&node.GameId, Champion: *&node.Champion,
			Season: *&node.Season, Role: *&node.Role, Lane: *&node.Lane,
			Queue: *&node.Queue, Timestamp: *&node.Timestamp, PlatformId: *&node.PlatformId}
		matchReferences = append(matchReferences, matchReference)
	}
	for _, element := range matchReferences {
		log.Println("-- PERSISTING MATCH LIST --")
		business_objects.PersistMatchReference(connection, element)
	}
}
