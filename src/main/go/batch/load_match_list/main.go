package main

import (
	"../../common/config"
	"./steps"
	"log"
	"time"
)

func main() {
	databaseConnection := config.GetDatabaseConnection()

	log.Println("------- LOADING MATCH LIST STARTED @", time.Now(), "-------")
	steps.LoadMatchList(databaseConnection)
	log.Println("------- LOADING MATCH LIST ENDED @", time.Now(), " -------")

	log.Println("CLOSING DATABASE CONNECTION")
	defer databaseConnection.Close()
	log.Println("DATABASE CONNECTION SUCCESSFULLY CLOSED")
}
