package steps

import (
	"../../../common/business_objects"
	databaseModel "../../../common/model"
	jsonModel "../../../common/model/json"
	"../../../common/utils"
	"database/sql"
	"encoding/json"
	"log"
)

func LoadChampions(connection *sql.DB) {
	//response := utils.GetRequest("https://euw1.api.riotgames.com/lol/static-data/v3/champions?" +
	//	"locale=en_US&champListData=all&tags=all&")
	response := utils.OpenFile("src/main/go/resources/json_samples/champions.json")
	var championsTree jsonModel.ChampionsTree

	var championsData []jsonModel.ChampionsDataTree

	log.Println("-- UNMARSHALLING CHAMPION LIST JSON --")
	err := json.Unmarshal(response, &championsTree)

	if err != nil {
		log.Println(err)
	} else {
		log.Println("-- CHAMPION LIST JSON UNMASHALLED --")
	}

	for _, element := range championsTree.DataTree {
		championsData = append(championsData, *element)
	}

	for _, element := range championsData {

		log.Println("-- ELABORATING CHAMPION STAT DATA --")
		championStatData := elabStatData(element)

		log.Println("-- ELABORATING CHAMPION PASSIVE DATA --")
		championPassiveData := elabPassiveData(element)

		log.Println("-- ELABORATING CHAMPION SPELL DATA --")
		championSpellsData := elabSpellsData(element)

		/* Check if the struct is empty */
		champ := business_objects.FindChampionById(connection, *&element.Id)
		if (databaseModel.Champions{} == champ) {
			log.Println("-- NO DATA, PERSISTING CHAMPION DATA --")

			log.Println("-- PERSISTING CHAMPION STATS DATA --")
			statId := business_objects.PersistChampionStats(connection, championStatData)
			log.Println("-- PERSISTING CHAMPION PASSIVE DATA --")
			passiveId := business_objects.PersistChampionPassive(connection, championPassiveData)

			for _, spellData := range championSpellsData {
				log.Println("-- PERSISTING CHAMPION SPELL DATA --")
				spellId := business_objects.PersistChampionSpell(connection, spellData)
				log.Println("-- PERSISTING CHAMPION DATA --")
				business_objects.PersistChampion(connection,
					elabChampionData(element, championsTree, statId, passiveId, spellId))
			}

		} else {
			log.Println("-- START UPDATING CHAMPION DATA --")
			championStat := business_objects.FindChampionStatByChampionId(connection, *&element.Id)
			championPass := business_objects.FindChampionPassiveByChampionId(connection, *&element.Id)

			// todo check change of key and patch version, if they're both changed move old stuff to historic
			championStatData.Id = championStat.Id
			championPassiveData.Id = championPass.Id
			business_objects.UpdateChampionStats(connection, championStatData)
			business_objects.UpdateChampionPassive(connection, championPassiveData)

			for _, spellData := range championSpellsData {
				championSpell := business_objects.FindChampionSpellsByChampionId(connection, *&element.Id)
				spellData.Id = championSpell.Id
				business_objects.UpdateChampionSpell(connection, spellData)
				business_objects.UpdateChampion(connection, databaseModel.Champions{}) // TODO to complete
			}
		}
	}
}

func elabStatData(element jsonModel.ChampionsDataTree) (databaseModel.ChampionStats) {
	return databaseModel.ChampionStats{Id: *&element.Id,
		ArmorPerLevel: *&element.Stats.ArmorPerLevel, AttackDamage: *&element.Stats.AttackDamage,
		ManaPointsPerLevel: *&element.Stats.ManaPointsPerLevel, AttackSpeedOffset: *&element.Stats.AttackSpeedOffset,
		ManaPoints: *&element.Stats.ManaPoints, Armor: *&element.Stats.Armor, HealthPoints: *&element.Stats.HealthPoints,
		HealthRegenPerLevel: *&element.Stats.HealthRegenPerLevel, AttackSpeedPerLevel: *&element.Stats.AttackSpeedPerLevel,
		AttackRange: *&element.Stats.AttackRange, MovementSpeed: *&element.Stats.MovementSpeed,
		AttackDamagePerLevel: *&element.Stats.AttackDamagePerLevel, ManaRegenPerLevel: *&element.Stats.ManaRegenPerLevel,
		CriticalDamagePerLevel: *&element.Stats.CriticalDamagePerLevel, MagicResistPerLevel: *&element.Stats.MagicResistPerLevel,
		CriticalDamage: *&element.Stats.CriticalDamage, ManaRegen: *&element.Stats.ManaRegen, MagicResist: *&element.Stats.MagicResist,
		HealthRegen: *&element.Stats.HealthRegen, HealthPointsPerLevel: *&element.Stats.HealthPointsPerLevel}
}

func elabPassiveData(element jsonModel.ChampionsDataTree) (databaseModel.ChampionPassive) {
	return databaseModel.ChampionPassive{Name: *&element.ChampionPassive.Name,
		SanitizedDescription: *&element.ChampionPassive.SanitizedDescription, FullImage: *&element.ChampionPassive.Image.FullImage,
		GroupImage: *&element.ChampionPassive.Image.GroupImage, ImageSprite: *&element.ChampionPassive.Image.ImageSprite,
		ImageHeight: *&element.ChampionPassive.Image.ImageHeight, ImageWidth: *&element.ChampionPassive.Image.ImageWidth,
		ImageY: *&element.ChampionPassive.Image.ImageY, ImageX: *&element.ChampionPassive.Image.ImageX,
		Description: *&element.ChampionPassive.Description}
}

func elabSpellsData(element jsonModel.ChampionsDataTree) []databaseModel.ChampionSpells {
	var spells []databaseModel.ChampionSpells
	for _, elementSpell := range element.ChampionSpells {
		var varsKey []string
		var varsCoeff []string
		var varsDyn []string
		var varsLink []string
		var varsRanksWith []string

		for _, elementVars := range elementSpell.Vars {
			varsKey = append(varsKey, *&elementVars.Key)
			varsCoeff = append(varsCoeff, utils.FloatArrayToStringWithSeparator(*&elementVars.Coeff))
			varsDyn = append(varsDyn, *&elementVars.Dyn)
			varsLink = append(varsLink, *&elementVars.Link)
			varsRanksWith = append(varsRanksWith, *&elementVars.RanksWith)
		}

		championSpellsData := databaseModel.ChampionSpells{SpellName: *&elementSpell.SpellName, SpellCooldown: *&elementSpell.SpellCooldown,
			Effect: utils.MatrixFloatArrayToStringWithSeparator(*&elementSpell.Effect),
			Cost: utils.IntArrayToStringWithSeparator(*&elementSpell.Cost),
			Cooldown: utils.FloatArrayToStringWithSeparator(*&elementSpell.Cooldown),
			SpellRange: utils.IntArrayToStringWithSeparator(*&elementSpell.SpellRange),
			CostType: *&elementSpell.CostType, SanitizedTooltip: *&elementSpell.SanitizedTooltip, Resource: *&elementSpell.Resource,
			EffectLevelTip: utils.StringArrayToStringWithSeparator(*&elementSpell.LevelTip.Effect),
			Label: utils.StringArrayToStringWithSeparator(*&elementSpell.LevelTip.Label),
			RanksWith: utils.StringArrayToStringWithSeparator(varsRanksWith),
			Dyn: utils.StringArrayToStringWithSeparator(varsDyn),
			Link: utils.StringArrayToStringWithSeparator(varsLink),
			Coeff: utils.StringArrayToStringWithSeparator(varsCoeff),
			KeyVars: utils.StringArrayToStringWithSeparator(varsKey),
			FullImage: *&elementSpell.Image.FullImage, GroupImage: *&elementSpell.Image.GroupImage, ImageSprite: *&elementSpell.Image.ImageSprite,
			ImageHeight: *&elementSpell.Image.ImageHeight, ImageWidth: *&elementSpell.Image.ImageWidth, ImageY: *&elementSpell.Image.ImageY,
			ImageX: *&elementSpell.Image.ImageX, SanitizedDescription: *&elementSpell.SanitizedDescription, Tooltip: *&elementSpell.Tooltip,
			MaxRank: *&elementSpell.MaxRank, CostBurn: *&elementSpell.CostBurn, RangeBurn: *&elementSpell.RangeBurn, Key: *&elementSpell.Key,
			Description: *&elementSpell.Description,
			EffectBurn: utils.StringArrayToStringWithSeparator(*&elementSpell.EffectBurn)}

		spells = append(spells, championSpellsData)
	}

	return spells
}

func elabChampionData(element jsonModel.ChampionsDataTree, championTree jsonModel.ChampionsTree,statId int64, passiveId int64, spellId int64)(databaseModel.Champions){
	return databaseModel.Champions{Id: *&element.Id, Name: *&element.Name, Title: *&element.Title, Version: championTree.Version,
		ChampionStatsId: statId, ChampionPassiveId: passiveId, ChampionSpellId: spellId, ChampionMasteryId:0, // TODO mastery Id
		EnemyTips: utils.StringArrayToStringWithSeparator(*&element.EnemyTips),
		Tags: utils.StringArrayToStringWithSeparator(*&element.Tags),
		ParType: *&element.ParType, Difficulty: *&element.Info.Difficulty, Attack: *&element.Info.Attack,
		Defense: *&element.Info.Defense, Magic: *&element.Info.Magic, FullImage: *&element.Image.FullImage,
		GroupImage: *&element.Image.GroupImage, ImageSprite: *&element.Image.ImageSprite, ImageHeight: *&element.Image.ImageHeight,
		ImageWidth: *&element.Image.ImageWidth, ImageY: *&element.Image.ImageY, ImageX: *&element.Image.ImageX,
		AllyTips: utils.StringArrayToStringWithSeparator(*&element.AllyTips),
		Key: *&element.Key, Lore: *&element.Lore, Blurb: *&element.Blurb}
}