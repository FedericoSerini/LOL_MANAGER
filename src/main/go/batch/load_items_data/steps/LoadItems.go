package steps

import (
	//"../../../common/http_utils"
	"../../../common/business_objects"
	databaseModel "../../../common/model"
	jsonModel "../../../common/model/json"
	"../../../common/utils"
	"database/sql"
	"encoding/json"
	"log"
)

func LoadItems(connection *sql.DB) {
	/* response := http_utils.GetRequest("https://euw1.api.riotgames.com/lol/static-data/v3/items?" +
	"locale=en_US&itemListData=all&tags=all&") */
	response := utils.OpenFile("src/main/go/resources/json_samples/items.json")
	var itemTree jsonModel.ItemsTree
	var itemList []databaseModel.Items

	log.Println("-- UNMARSHALLING ITEM LIST JSON --")
	err := json.Unmarshal(response, &itemTree)

	if err != nil {
		log.Println(err)
	} else {
		log.Println("-- ITEM LIST JSON UNMASHALLED --")
	}
	/*
		for _, node := range itemTree.ItemsDataTree {
			item := databaseModel.Items{ID: *&node.ID, Name: *&node.Name,
				Plaintext: *&node.Plaintext, SanitizedDescription: *&node.SanitizedDescription,
				Base: *&node.Gold.Base, Sell: *&node.Gold.Sell, Total: *&node.Gold.Total}
			itemList = append(itemList, item)
		} */

	for _, node := range itemTree.ItemsDataTree {
		log.Println(*&node)
	}

	for _, element := range itemList {
		log.Println(element)
		business_objects.PersistItem(connection, element)
	}
}
