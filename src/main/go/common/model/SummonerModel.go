package model

type Summoner struct {
	ProfileIconId int
	Name          string
	SummonerLevel int32
	RevisionDate  int64 // Date summoner was last modified specified as epoch milliseconds
	Id            int32
	AccountId     int32
}
