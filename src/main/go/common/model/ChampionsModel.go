package model

type Champions struct {
	Id                	int64
	Name              	string
	Title            	string
	Version 		  	string
	ChampionStatsId   	int64
	ChampionPassiveId 	int64
	ChampionSpellId   	int64
	ChampionMasteryId 	int64
	EnemyTips       	string
	Tags            	string
	ParType         	string
	Difficulty 			int
	Attack     			int
	Defense    			int
	Magic      			int
	FullImage   		string
	GroupImage  		string
	ImageSprite 		string
	ImageHeight 		int
	ImageWidth  		int
	ImageY      		int
	ImageX      		int
	AllyTips        	string
	Key             	string
	Lore            	string
	Blurb           	string
}
