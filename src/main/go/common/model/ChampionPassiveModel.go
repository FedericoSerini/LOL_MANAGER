package model

type ChampionPassive struct {
	Id 					 int64
	Name                 string
	SanitizedDescription string
	FullImage            string
	GroupImage           string
	ImageSprite          string
	ImageHeight          int
	ImageWidth           int
	ImageY               int
	ImageX               int
	Description          string
}
