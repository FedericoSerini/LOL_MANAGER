package model

type ChampionStats struct {
	Id                     int64
	ArmorPerLevel          float64
	AttackDamage           float64
	ManaPointsPerLevel     float64
	AttackSpeedOffset      float64
	ManaPoints             float64
	Armor                  float64
	HealthPoints           float64
	HealthRegenPerLevel    float64
	AttackSpeedPerLevel    float64
	AttackRange            float64
	MovementSpeed          float64
	AttackDamagePerLevel   float64
	ManaRegenPerLevel      float64
	CriticalDamagePerLevel float64
	MagicResistPerLevel    float64
	CriticalDamage         float64
	ManaRegen              float64
	MagicResist            float64
	HealthRegen            float64
	HealthPointsPerLevel   float64
}
