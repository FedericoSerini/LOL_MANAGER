package model

type League struct {
	Id               int64
	Rank             string
	QueueType        string
	HotStreak        bool
	MiniSeries       int32
	Wins             int
	Veteran          bool
	Losses           int
	FreshBlood       bool
	LeagueId         string
	PlayerOrTeamName string
	Inactive         bool
	PlayerOrTeamId   string
	LeagueName       string
	Tier             string
	LeaguePoints     int
}

// PROMO (?)
type MiniSeries struct {
	Wins     int
	Losses   int
	Target   int
	Progress string
}
