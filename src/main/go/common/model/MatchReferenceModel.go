package model

// N MatchReferenceModel have 1 MatchListModel
type MatchReference struct {
	Lane       string
	GameId     int64 // match id, PK
	Champion   int
	PlatformId string
	Timestamp  int64
	Queue      int32
	Role       string
	Season     int
}
