package json

type ChampionMasteryTree []struct {
	ChestGranted                 bool  `json:"chestGranted"`                 // Is chest granted for this champion or not in current season.
	ChampionLevel                int   `json:"championLevel"`                // Champion level for specified player and champion combination.
	ChampionPoints               int   `json:"championPoints"`               // Total number of champion points for this player and champion combination - they are used to determine championLevel.
	ChampionId                   int32 `json:"championId"`                   // Champion ID for this entry.
	PlayerId                     int32 `json:"playerId"`                     // Player ID for this entry.
	ChampionPointsUntilNextLevel int32 `json:"championPointsUntilNextLevel"` // Number of points needed to achieve next level. Zero if player reached maximum champion level for this champion.
	TokensEarned                 int   `json:"tokensEarned"`                 // The token earned for this champion to level up.
	ChampionPointsSinceLastLevel int32 `json:"championPointsSinceLastLevel"` // Number of points earned since current level has been achieved.
	LastPlayTime                 int64 `json:"lastPlayTime"`                 // Last time this champion was played by this player - in Unix milliseconds time format.
}
