package json

type MatchTree struct {
	SeasonId              int                                 `json:"seasonId"`
	QueueId               int64                               `json:"queueId"`
	GameId                int64                               `json:"gameId"`
	ParticipantIdentities map[string]*ParticipantIdentityTree `json:"participantIdentities"`
	GameVersion           string                              `json:"gameVersion"`
	PlatformId            string                              `json:"platformId"`
	GameMode              string                              `json:"gameMode"`
	MapId                 int64                               `json:"mapId"`
	GameType              string                              `json:"gameType"`
	Teams                 map[string]*TeamStatsTree           `json:"teams"`
	Participants          map[string]*ParticipantTree         `json:"participants"`
	GameDuration          int32                               `json:"gameDuration"`
	GameCreation          int32                               `json:"gameCreation"`
}

type ParticipantIdentityTree struct {
	Player        PlayerTree `json:"player"`
	ParticipantId int64      `json:"participantId"`
}

type PlayerTree struct {
	CurrentPlatformId string `json:"currentPlatformId"`
	SummonerName      string `json:"summonerName"`
	MatchHistoryUri   string `json:"matchHistoryUri"`
	PlatformId        string `json:"platformId"`
	CurrentAccountId  int32  `json:"currentAccountId"`
	ProfileIcon       int    `json:"profileIcon"`
	SummonerId        int32  `json:"summonerId"`
	AccountId         int32  `json:"accountId"`
}

type TeamStatsTree struct {
	FirstDragon          bool                     `json:"firstDragon"`
	FirstInhibitor       bool                     `json:"firstInhibitor"`
	Bans                 map[string]*TeamBansTree `json:"bans"`
	BaronKills           int                      `json:"baronKills"`
	FirstRiftHerald      bool                     `json:"firstRiftHerald"`
	FirstBaron           bool                     `json:"firstBaron"`
	RiftHeraldKills      int                      `json:"riftHeraldKills"`
	FirstBlood           bool                     `json:"firstBlood"`
	TeamId               int64                    `json:"teamId"`
	FirstTower           bool                     `json:"firstTower"`
	VilemawKills         int                      `json:"vilemawKills"`
	InhibitorKills       int                      `json:"inhibitorKills"`
	TowerKills           int                      `json:"towerKills"`
	DominionVictoryScore int                      `json:"dominionVictoryScore"`
	Win                  string                   `json:"win"`
	DragonKills          int                      `json:"dragonKills"`
}

type TeamBansTree struct {
	PickTurn   int   `json:"pickTurn"`
	ChampionId int64 `json:"championId"`
}

type ParticipantTree struct {
	Stats                     ParticipantStatsTree    `json:"stats"`
	ParticipantId             int64                   `json:"participantId"`
	Runes                     map[string]*RuneTree    `json:"runes"`
	Timeline                  ParticipantTimelineTree `json:"timeline"`
	TeamId                    int64                   `json:"teamId"`
	Spell2Id                  int64                   `json:"spell2Id"`
	Masteries                 map[string]*MasteryTree `json:"masteries"`
	HighestAchievedSeasonTier string                  `json:"highestAchievedSeasonTier"`
	Spell1Id                  int64                   `json:"spell1Id"`
	ChampionId                int64                   `json:"championId"`
}

type ParticipantStatsTree struct {
	PhysicalDamageDealt             int32 `json:"physicalDamageDealt"`
	NeutralMinionsKilledTeamJungle  int   `json:"neutralMinionsKilledTeamJungle"`
	MagicDamageDealt                int32 `json:"magicDamageDealt"`
	TotalPlayerScore                int64 `json:"totalPlayerScore"`
	Deaths                          int   `json:"deaths"`
	Win                             bool  `json:"win"`
	NeutralMinionsKilledEnemyJungle int   `json:"neutralMinionsKilledEnemyJungle"`
	AltarsCaptured                  int   `json:"altarsCaptured"`
	LargestCriticalStrike           int   `json:"largestCriticalStrike"`
	TotalDamageDealt                int32 `json:"totalDamageDealt"`
	MagicDamageDealtToChampions     int32 `json:"magicDamageDealtToChampions"`
	VisionWardsBoughtInGame         int   `json:"visionWardsBoughtInGame"`
	DamageDealtToObjectives         int32 `json:"damageDealtToObjectives"`
	LargestKillingSpree             int   `json:"largestKillingSpree"`
	Item1                           int64 `json:"item1"`
	QuadraKills                     int   `json:"quadraKills"`
	TeamObjective                   int   `json:"teamObjective"`
	TotalTimeCrowdControlDealt      int   `json:"totalTimeCrowdControlDealt"`
	Int32estTimeSpentLiving         int32 `json:"int32estTimeSpentLiving"`
	WardsKilled                     int   `json:"wardsKilled"`
	FirstTowerAssist                bool  `json:"firstTowerAssist"`
	FirstTowerKill                  bool  `json:"firstTowerKill"`
	Item2                           int64 `json:"item2"`
	Item3                           int64 `json:"item3"`
	Item0                           int64 `json:"item0"`
	FirstBloodAssist                bool  `json:"firstBloodAssist"`
	VisionScore                     int32 `json:"visionScore"`
	WardsPlaced                     int   `json:"wardsPlaced"`
	Item4                           int64 `json:"item4"`
	Item5                           int64 `json:"item5"`
	Item6                           int64 `json:"item6"`
	TurretKills                     int   `json:"turretKills"`
	TripleKills                     int   `json:"tripleKills"`
	DamageSelfMitigated             int32 `json:"damageSelfMitigated"`
	ChampLevel                      int   `json:"champLevel"`
	NodeNeutralizeAssist            int   `json:"nodeNeutralizeAssist"`
	FirstInhibitorKill              bool  `json:"firstInhibitorKill"`
	GoldEarned                      int64 `json:"goldEarned"`
	MagicalDamageTaken              int32 `json:"magicalDamageTaken"`
	Kills                           int   `json:"kills"`
	DoubleKills                     int   `json:"doubleKills"`
	NodeCaptureAssist               int   `json:"nodeCaptureAssist"`
	TrueDamageTaken                 int32 `json:"trueDamageTaken"`
	NodeNeutralize                  int   `json:"nodeNeutralize"`
	FirstInhibitorAssist            bool  `json:"firstInhibitorAssist"`
	Assists                         int   `json:"assists"`
	UnrealKills                     int   `json:"unrealKills"`
	NeutralMinionsKilled            int   `json:"neutralMinionsKilled"`
	ObjectivePlayerScore            int32 `json:"objectivePlayerScore"`
	CombatPlayerScore               int   `json:"combatPlayerScore"`
	DamageDealtToTurrets            int32 `json:"damageDealtToTurrets"`
	AltarsNeutralized               int   `json:"altarsNeutralized"`
	PhysicalDamageDealtToChampions  int32 `json:"physicalDamageDealtToChampions"`
	GoldSpent                       int64 `json:"goldSpent"`
	TrueDamageDealt                 int32 `json:"trueDamageDealt"`
	TrueDamageDealtToChampions      int32 `json:"trueDamageDealtToChampions"`
	ParticipantId                   int64 `json:"participantId"`
	PentaKills                      int   `json:"pentaKills"`
	TotalHeal                       int32 `json:"totalHeal"`
	TotalMinionsKilled              int   `json:"totalMinionsKilled"`
	FirstBloodKill                  bool  `json:"firstBloodKill"`
	NodeCapture                     int   `json:"nodeCapture"`
	LargestMultiKill                int   `json:"largestMultiKill"`
	SightWardsBoughtInGame          int   `json:"sightWardsBoughtInGame"`
	TotalDamageDealtToChampions     int32 `json:"totalDamageDealtToChampions"`
	TotalUnitsHealed                int64 `json:"totalUnitsHealed"`
	InhibitorKills                  int   `json:"inhibitorKills"`
	TotalScoreRank                  int   `json:"totalScoreRank"`
	TotalDamageTaken                int32 `json:"totalDamageTaken"`
	KillingSprees                   int   `json:"killingSprees"`
	TimeCCingOthers                 int32 `json:"timeCCingOthers"`
	PhysicalDamageTaken             int32 `json:"physicalDamageTaken"`
}

type RuneTree struct {
	RuneId int `json:"runeId"`
	Rank   int `json:"rank"`
}

type ParticipantTimelineTree struct {
	Lane                        string             `json:"lane"`
	ParticipantId               int                `json:"participantId"`
	CsDiffPerMinDeltas          map[string]float64 `json:"csDiffPerMinDeltas"`
	GoldPerMinDeltas            map[string]float64 `json:"goldPerMinDeltas"`
	XpDiffPerMinDeltas          map[string]float64 `json:"xpDiffPerMinDeltas"`
	CreepsPerMinDeltas          map[string]float64 `json:"creepsPerMinDeltas"`
	XpPerMinDeltas              map[string]float64 `json:"xpPerMinDeltas"`
	Role                        string             `json:"role"`
	DamageTakenDiffPerMinDeltas map[string]float64 `json:"damageTakenDiffPerMinDeltas"`
	DamageTakenPerMinDeltas     map[string]float64 `json:"damageTakenPerMinDeltas"`
}

type MasteryTree struct {
	MasteryId int `json:"masteryId"`
	Rank      int `json:"rank"`
}
