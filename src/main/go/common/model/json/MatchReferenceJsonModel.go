package json

type MatchListTree struct {
	Matches    MatchReferenceTree `json:"matches"`
	EndIndex   int                `json:"endIndex"`
	StartIndex int                `json:"startIndex"`
	TotalGames int                `json:"totalGames"`
}

type MatchReferenceTree []struct {
	Lane       string `json:"lane"`
	GameId     int64  `json:"gameId"` // match id
	Champion   int64  `json:"champion"`
	PlatformId string `json:"platformId"`
	Timestamp  int64  `json:"timestamp"`
	Queue      int32  `json:"queue"`
	Role       string `json:"role"`
	Season     int    `json:"season"`
}
