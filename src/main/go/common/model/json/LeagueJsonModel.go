package json

type LeagueTree []struct {
	Rank             string                     `json:"rank"`
	QueueType        string                     `json:"queueType"`
	HotStreak        bool                       `json:"hotStreak"`
	MiniSeries       map[string]*MiniSeriesTree `json:"miniSeries"`
	Wins             int                        `json:"wins"`
	Veteran          bool                       `json:"veteran"`
	Losses           int                        `json:"losses"`
	FreshBlood       bool                       `json:"freshBlood"`
	LeagueId         string                     `json:"leagueId"`
	PlayerOrTeamName string                     `json:"playerOrTeamName"`
	Inactive         bool                       `json:"inactive"`
	PlayerOrTeamId   string                     `json:"playerOrTeamId"`
	LeagueName       string                     `json:"leagueName"`
	Tier             string                     `json:"tier"`
	LeaguePoints     int                        `json:"leaguePoints"`
}

// PROMO (?)
type MiniSeriesTree struct {
	Wins     int    `json:"wins"`
	Losses   int    `json:"losses"`
	Target   int    `json:"target"`
	Progress string `json:"progress"`
}
