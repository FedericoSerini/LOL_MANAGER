package json

type Summoner struct {
	ProfileIconId int    `json:"profileIconId"`
	Name          string `json:"name"`
	SummonerLevel int32  `json:"summonerLevel"`
	RevisionDate  int64  `json:"revisionDate"` // Date summoner was last modified specified as epoch milliseconds
	Id            int32  `json:"id"`
	AccountId     int32  `json:"accountId"`
}
