package json

type ChampionsTree struct {
	DataTree map[string]*ChampionsDataTree `json:"data"`
	Type     string                        `json:"type"`
	Version  string                        `json:"version"`
	Keys     map[string]string             `json:"keys"`
	Format   string                        `json:"format"`
}

type ChampionsDataTree struct {
	Id              int64               `json:"id"`
	Stats           ChampionStatsTree   `json:"stats"`
	Name            string              `json:"name"`
	Title           string              `json:"title"`
	ChampionPassive ChampionPassiveTree `json:"passive"`
	ChampionSpells  ChampionSpellsTree  `json:"spells"`
	Info            ChampionInfoTree    `json:"info"`
	EnemyTips       []string            `json:"enemytips"`
	Image           ChampionImageTree   `json:"image"`
	Tags            []string            `json:"tags"`
	ParType         string              `json:"partype"`
	Skins           ChampionSkinTree    `json:"skins"`
	Recommended     ChampionReccTree    `json:"recommended"`
	AllyTips        []string            `json:"allytips"`
	Key             string              `json:"key"`
	Lore            string              `json:"lore"`
	Blurb           string              `json:"blurb"`
}

type ChampionStatsTree struct {
	ArmorPerLevel          float64 `json:"armorperlevel"`
	AttackDamage           float64 `json:"attackdamage"`
	ManaPointsPerLevel     float64 `json:"mpperlevel"`
	AttackSpeedOffset      float64 `json:"attackspeedoffset"`
	ManaPoints             float64 `json:"mp"`
	Armor                  float64 `json:"armor"`
	HealthPoints           float64 `json:"hp"`
	HealthRegenPerLevel    float64 `json:"hpregenperlevel"`
	AttackSpeedPerLevel    float64 `json:"attackspeedperlevel"`
	AttackRange            float64 `json:"attackrange"`
	MovementSpeed          float64 `json:"movespeed"`
	AttackDamagePerLevel   float64 `json:"attackdamageperlevel"`
	ManaRegenPerLevel      float64 `json:"mpregenperlevel"`
	CriticalDamagePerLevel float64 `json:"critperlevel"`
	MagicResistPerLevel    float64 `json:"spellblockperlevel"`
	CriticalDamage         float64 `json:"crit"`
	ManaRegen              float64 `json:"mpregen"`
	MagicResist            float64 `json:"spellblock"`
	HealthRegen            float64 `json:"hpregen"`
	HealthPointsPerLevel   float64 `json:"hpperlevel"`
}

type ChampionPassiveTree struct {
	Name                 string            `json:"name"`
	SanitizedDescription string            `json:"sanitizedDescription"`
	Image                ChampionImageTree `json:"image"`
	Description          string            `json:"description"`
}

type ChampionSpellsTree []struct {
	SpellName            string              `json:"name"`
	SpellCooldown        string              `json:"cooldownBurn"`
	Effect               [][]float64         `json:"effect"`   /* an array of an ability’s values per level */
	Cost                 []int               `json:"cost"`     /* an array of an ability’s cost per level */
	Cooldown             []float64           `json:"cooldown"` /* an array of ability’s cooldown per level */
	SpellRange           []int               `json:"range"`    /* an array of ability’s range per level */
	CostType             string              `json:"costType"` /* Mana Or Energy */
	SanitizedTooltip     string              `json:"sanitizedTooltip"`
	Resource             string              `json:"resource"`
	LevelTip             LevelTipTree        `json:"leveltip"`
	Vars                 SpellVarsTree       `json:"vars"`
	Image                ChampionImageTree   `json:"image"`
	SanitizedDescription string              `json:"sanitizedDescription"`
	Tooltip              string              `json:"tooltip"`
	MaxRank              int                 `json:"maxrank"`
	CostBurn             string              `json:"costBurn"`
	RangeBurn            string              `json:"rangeBurn"`
	Key                  string              `json:"key"`
	Description          string              `json:"description"`
	EffectBurn           []string            `json:"effectBurn"`
	AltImages            []ChampionImageTree `json:"altimages"`
}

type ChampionInfoTree struct {
	Difficulty int `json:"difficulty"`
	Attack     int `json:"attack"`
	Defense    int `json:"defense"`
	Magic      int `json:"magic"`
}

type ChampionSkinTree []struct {
	Num  int    `json:"num"`
	Name string `json:"name"`
	Id   int    `json:"id"`
}

type ChampionImageTree struct {
	FullImage   string `json:"full"`
	GroupImage  string `json:"group"`
	ImageSprite string `json:"sprite"`
	ImageHeight int    `json:"h"`
	ImageWidth  int    `json:"w"`
	ImageY      int    `json:"y"`
	ImageX      int    `json:"x"`
}

type ChampionReccTree []struct {
	Map      string            `json:"map"`
	Blocks   ChampionBlockTree `json:"blocks"`
	Champion string            `json:"champion"`
	Title    string            `json:"title"`
	Priority bool              `json:"priority"`
	Mode     string            `json:"mode"`
	Type     string            `json:"type"`
}

type ChampionBlockTree []struct {
	Items   ItemBlockTree `json:"items"`
	RecMath bool          `json:"recMath"`
	Type    string        `json:"type"`
}

type ItemBlockTree []struct {
	Count int `json:"count"`
	Id    int `json:"id"`
}

type LevelTipTree struct {
	Effect []string `json:"effect"`
	Label  []string `json:"label"`
}

type SpellVarsTree []struct {
	RanksWith string    `json:"ranksWith"`
	Dyn       string    `json:"dyn"`
	Link      string    `json:"link"`
	Coeff     []float64 `json:"coeff"`
	Key       string    `json:"key"`
}
