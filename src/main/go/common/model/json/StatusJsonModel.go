package json

type StatusesTree struct {
	Name      string      `json:"name"`
	RegionTag string      `json:"region_tag"`
	Hostname  string      `json:"hostname"`
	Services  ServiceTree `json:"services"`
	Slug      string      `json:"slug"`
	Locales   []string    `json:"locales"`
}

type ServiceTree []struct {
	Status    string       `json:"status"`
	Incidents IncidentTree `json:"incidents"`
	Name      string       `json:"name"`
	Slug      string       `json:"slug"`
}

type IncidentTree []struct {
	Active    bool        `json:"active"`
	CreatedAt string      `json:"created_at"`
	Id        int64       `json:"id"`
	Updates   MessageTree `json:"updates"`
}

type MessageTree []struct {
	Severity     string          `json:"severity"`
	Author       string          `json:"author"`
	CreatedAt    string          `json:"created_at"`
	Translations TranslationTree `json:"translations"`
	UpdatedAt    string          `json:"updated_at"`
	Content      string          `json:"content"`
	Id           string          `json:"id"`
}

type TranslationTree []struct {
	Locale    string `json:"locale"`
	Content   string `json:"content"`
	UpdatedAt string `json:"updated_at"`
}
