package json

type ItemsTree struct {
	ItemsDataTree map[string]*ItemsDataTree `json:"data"`
	Groups        Groups                    `json:"groups"`
	Tree          Tree                      `json:"tree"`
	Type          string                    `json:"type"`
	Version       string                    `json:"version"`
}

type ItemsDataTree struct {
	ID                   int64             `json:"id"`
	Name                 string            `json:"name"`
	Plaintext            string            `json:"plaintext"`
	SanitizedDescription string            `json:"sanitizedDescription"`
	Stats                StatsTree         `json:"stats"`
	Gold                 GoldTree          `json:"gold"`
	Image                ItemImageTree     `json:"image"`
	Colloq               string            `json:"colloq"`
	MapsTree             MapsTree          `json:"maps"`
	IntoTree             []string          `json:"into"`
	FromTree             []string          `json:"from"`
	ItemTagsTree         []string          `json:"tags"`
	Depth                int               `json:"depth"`
	EffectTree           map[string]string `json:"effect"`
	Stacks               int               `json:"stacks"`
	HideFromAll          bool              `json:"hideFromAll"`
	InStore              bool              `json:"inStore"`
	SpecialRecipe        int               `json:"specialRecipe"`
	RequiredChampion     string            `json:"requiredChampion"`
	ConsumeOnFull        bool              `json:"consumeOnFull"`
	Consumed             bool              `json:"consumed"`
}

type StatsTree struct {
	PercentCritDamageMod     float64 `json:"PercentCritDamageMod"`
	PercentSpellBlockMod     float64 `json:"PercentSpellBlockMod"`
	PercentHPRegenMod        float64 `json:"PercentHPRegenMod"`
	PercentMovementSpeedMod  float64 `json:"PercentMovementSpeedMod"`
	FlatSpellBlockMod        float64 `json:"FlatSpellBlockMod"`
	FlatCritDamageMod        float64 `json:"FlatCritDamageMod"`
	FlatEnergyPoolMod        float64 `json:"FlatEnergyPoolMod"`
	PercentLifeStealMod      float64 `json:"PercentLifeStealMod"`
	FlatMPPoolMod            float64 `json:"FlatMPPoolMod"`
	FlatMovementSpeedMod     float64 `json:"FlatMovementSpeedMod"`
	PercentAttackSpeedMod    float64 `json:"PercentAttackSpeedMod"`
	FlatBlockMod             float64 `json:"FlatBlockMod"`
	PercentBlockMod          float64 `json:"PercentBlockMod"`
	FlatEnergyRegenMod       float64 `json:"FlatEnergyRegenMod"`
	PercentSpellVampMod      float64 `json:"PercentSpellVampMod"`
	FlatMPRegenMod           float64 `json:"FlatMPRegenMod"`
	PercentDodgeMod          float64 `json:"PercentDodgeMod"`
	FlatAttackSpeedMod       float64 `json:"FlatAttackSpeedMod"`
	FlatArmorMod             float64 `json:"FlatArmorMod"`
	FlatHPRegenMod           float64 `json:"FlatHPRegenMod"`
	PercentMagicDamageMod    float64 `json:"PercentMagicDamageMod"`
	PercentMPPoolMod         float64 `json:"PercentMPPoolMod"`
	FlatMagicDamageMod       float64 `json:"FlatMagicDamageMod"`
	PercentMPRegenMod        float64 `json:"PercentMPRegenMod"`
	PercentPhysicalDamageMod float64 `json:"PercentPhysicalDamageMod"`
	FlatPhysicalDamageMod    float64 `json:"FlatPhysicalDamageMod"`
	PercentHPPoolMod         float64 `json:"PercentHPPoolMod"`
	PercentArmorMod          float64 `json:"PercentArmorMod"`
	PercentCritChanceMod     float64 `json:"PercentCritChanceMod"`
	PercentEXPBonus          float64 `json:"PercentEXPBonus"`
	FlatHPPoolMod            float64 `json:"FlatHPPoolMod"`
	FlatCritChanceMod        float64 `json:"FlatCritChanceMod"`
	FlatEXPBonus             float64 `json:"FlatEXPBonus"`
}

type GoldTree struct {
	Base        int64 `json:"base"`
	Purchasable bool  `json:"purchasable"`
	Sell        int64 `json:"sell"`
	Total       int64 `json:"total"`
}

type ItemImageTree struct {
	FullImage   string `json:"full"`
	GroupImage  string `json:"group"`
	ImageSprite string `json:"sprite"`
	ImageHeight int32  `json:"h"`
	ImageWidth  int32  `json:"w"`
	ImageY      int32  `json:"y"`
	ImageX      int32  `json:"x"`
}

// https://developer.riotgames.com/game-constants.html
type MapsTree struct {
	SummonerRiftSummerVariant        bool `json:"1"`
	SummonerRiftAutumnVariant        bool `json:"2"`
	ProvingGroundsTutorialMap        bool `json:"3"`
	TwistedTreelineOriginal          bool `json:"4"`
	TheCrystalScarDominion           bool `json:"8"`
	TwistedTreelineCurrent           bool `json:"10"`
	SummonerRiftAutumnVariantCurrent bool `json:"11"`
	HowlingAbyssARAM                 bool `json:"12"`
	ButchersBridgeARAM               bool `json:"14"`
	CosmicRuinsDarkStar              bool `json:"16"`
	ValoranCityParkStarGuardian      bool `json:"18"`
	SubstructurePROJECT              bool `json:"19"`
}

type Groups []struct {
	MaxGroupOwnable string `json:"MaxGroupOwnable"`
	Key             string `json:"key"`
}

type Tree []struct {
	Header string   `json:"header"`
	Tags   []string `json:"tags"`
}
