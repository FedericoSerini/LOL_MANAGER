package model

type ChampionMastery struct {
	Id                           int64
	ChestGranted                 bool  // Is chest granted for this champion or not in current season.
	ChampionLevel                int   // Champion level for specified player and champion combination.
	ChampionPoints               int   // Total number of champion points for this player and champion combination - they are used to determine championLevel.
	ChampionId                   int32 // Champion ID for this entry.
	PlayerId                     int32 // Player ID for this entry.
	ChampionPointsUntilNextLevel int32 // Number of points needed to achieve next level. Zero if player reached maximum champion level for this champion.
	TokensEarned                 int   // The token earned for this champion to level up.
	ChampionPointsSinceLastLevel int32 // Number of points earned since current level has been achieved.
	LastPlayTime                 int64 // Last time this champion was played by this player - in Unix milliseconds time format.
}
