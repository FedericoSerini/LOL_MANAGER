package model

type Items struct {
	ID                   int
	Name                 string
	Plaintext            string
	SanitizedDescription string
	Base                 int
	Sell                 int
	Total                int
}
