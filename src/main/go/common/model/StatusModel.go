package model

type Status struct {
	Name      string
	RegionTag string
	Hostname  string
	Services  Service
	Slug      string
	Locales   []string
}

type Service []struct {
	Status    string
	Incidents Incident
	Name      string
	Slug      string
}

type Incident []struct {
	Active    bool
	CreatedAt string
	Id        int64
	Updates   Message
}

type Message []struct {
	Severity     string
	Author       string
	CreatedAt    string
	Translations Translation
	UpdatedAt    string
	Content      string
	Id           string
}

type Translation []struct {
	Locale    string
	Content   string
	UpdatedAt string
}
