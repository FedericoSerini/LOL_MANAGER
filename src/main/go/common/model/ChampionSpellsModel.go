package model

type ChampionSpells struct {
	Id			 		 int64
	SpellName            string
	SpellCooldown        string
	Effect               string
	Cost                 string
	Cooldown             string
	SpellRange           string
	CostType             string
	SanitizedTooltip     string
	Resource             string
	EffectLevelTip       string
	Label                string
	RanksWith            string
	Dyn                  string
	Link                 string
	Coeff                string
	KeyVars              string
	FullImage            string
	GroupImage           string
	ImageSprite          string
	ImageHeight          int
	ImageWidth           int
	ImageY               int
	ImageX               int
	SanitizedDescription string
	Tooltip              string
	MaxRank              int
	CostBurn             string
	RangeBurn            string
	Key                  string
	Description          string
	EffectBurn           string
	//AltImages				[]ChampionImageTree
}
