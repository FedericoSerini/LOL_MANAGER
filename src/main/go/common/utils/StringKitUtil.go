package utils

import (
	"bytes"
	"strconv"
	"strings"
)

var buffer bytes.Buffer

func StringArrayToStringWithSeparator(stringToFill []string) string {
	for _, node := range stringToFill {
		buffer.WriteString(node + ";")
	}
	return buffer.String()
}

func IntArrayToStringWithSeparator(intToFill []int) string {
	for _, node := range intToFill {
		i64 := int64(node)
		parsedInt := strconv.FormatInt(i64, 10)
		buffer.WriteString(parsedInt + ";")
	}
	return buffer.String()
}

func FloatArrayToStringWithSeparator(floatToFill []float64) string {
	for _, node := range floatToFill {
		parsedFloat := strconv.FormatFloat(node, 'E', -1, 32)
		buffer.WriteString(parsedFloat + ";")
	}
	return buffer.String()
}

func MatrixFloatArrayToStringWithSeparator(floatToFill [][]float64) string {
	for i := 0; i < len(floatToFill); i++ {
		for j := 0; j < len(floatToFill[i]); j++ {
			parsedMatrixFloat := strconv.FormatFloat(floatToFill[i][j], 'E', -1, 32)
			buffer.WriteString(parsedMatrixFloat + ";")
		}
	}
	return buffer.String()
}

func StringToArrayFromSeparator(stringToTokenize string) []string {
	return strings.Split(stringToTokenize, ";")
}
