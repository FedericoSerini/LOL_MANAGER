package utils

import (
	"fmt"
	"io/ioutil"
)

func OpenFile(path string) []byte {
	data, err := ioutil.ReadFile(path)

	if err != nil {
		fmt.Println(err)
		return nil
	} else {
		return data
	}
}
