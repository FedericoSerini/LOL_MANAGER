package utils

import (
	"io/ioutil"
	"log"
	"net/http"
)

var apiKey = "API_KEY_HERE"

func GetRequest(path string) []byte {
	client := &http.Client{}
	request, errorCreatingRequest := http.NewRequest("GET", path+apiKey, nil)
	if errorCreatingRequest != nil {
		log.Println("ERROR CREATING GET REQUEST FOR URI: ", path, "DUE:", errorCreatingRequest)
	}
	request.Header.Add("Accept", "application/json")
	response, errorOnResponse := client.Do(request)
	defer response.Body.Close()
	bodyInBytes, errorOnReadingBody := ioutil.ReadAll(response.Body)
	if errorOnReadingBody != nil {
		log.Println("ERROR ON READING RESPONSE BODY DUE:", errorOnReadingBody)
	}
	if errorOnResponse != nil || response.StatusCode != 200 {
		log.Println("ERROR EXECUTING GET REQUEST FOR URI: ", path, "DUE:", string(bodyInBytes))
	}
	return bodyInBytes
}
