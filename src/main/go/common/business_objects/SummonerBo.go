package business_objects

import (
	"../model"
	"database/sql"
	"log"
)

func PersistSummoner(connection *sql.DB, summoner model.Summoner) {
	query := "INSERT INTO summoner (profile_icon_id, name, summoner_level, revision_date, id, account_id) VALUES (?,?,?,?,?,?)"
	_, err := connection.Exec(query, summoner.ProfileIconId, summoner.Name, summoner.SummonerLevel, summoner.RevisionDate, summoner.Id, summoner.AccountId)

	if err != nil {
		log.Println("ERROR PersistSummoner: ", err)
	}
}

func FindSummonerById(connection *sql.DB, summonerId int32) {
	query := "SELECT * FROM summoner WHERE id = ?"
	connection.Exec(query, summonerId)
}

func FindSummonerByName(connection *sql.DB, summonerName string) {
	query := "SELECT * FROM summoner WHERE name = ?"
	connection.Exec(query, summonerName)
}
