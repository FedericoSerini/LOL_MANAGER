package business_objects

import (
	"../model"
	"database/sql"
	"log"
)

func PersistAlertStatus(connection *sql.DB, status model.Status) {
	query := "INSERT INTO server_alert_status (id,name,description,sanitized_description,base_gold_price,total_full_item_gold,sell_gold_price) VALUES (?,?,?,?,?,?,?)"
	_, err := connection.Exec(query) //todo add params and correct query

	if err != nil {
		log.Println("ERROR PersistAlertStatus: ", err)
	}
}
