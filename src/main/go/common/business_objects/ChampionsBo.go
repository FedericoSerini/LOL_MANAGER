package business_objects

import (
	"../model"
	"database/sql"
	"log"
)

func PersistChampion(connection *sql.DB, champion model.Champions) {
	query := "INSERT INTO champions (id,stat_id,name,title,passive_id,spell_id) VALUES (?,?,?,?,?,?)"
	_, err := connection.Exec(query,
		champion.Id,
		champion.ChampionStatsId,
		champion.Name,
		champion.Title,
		champion.ChampionPassiveId,
		champion.ChampionSpellId)

	if err != nil {
		log.Println("ERROR PersistChampion: ", err)
	}
}

func UpdateChampion(connection *sql.DB, championStats model.Champions) {
	query := "UPDATE champions_stats SET armor_per_level=?, attack_damage = ?, mana_points_per_level = ?, attack_speed_offset = ?, mana_points = ?, armor = ?, health_points = ?, health_regen_per_level = ?, attack_speed_per_level = ?, attack_range = ?, movement_speed = ?, attack_damage_per_level = ?, mana_regen_per_level = ?, critical_damage_per_level = ?, magic_resist_per_level = ?, critical_damage = ?, mana_regen = ?, magic_resist = ?, health_regen = ?, health_points_per_level = ? WHERE id = ?"
	_, err := connection.Exec(query,
		championStats.ArmorPerLevel, championStats.AttackDamage,
		championStats.ManaPointsPerLevel, championStats.AttackSpeedOffset,
		championStats.ManaPoints, championStats.Armor, championStats.HealthPoints,
		championStats.HealthRegenPerLevel, championStats.AttackSpeedPerLevel,
		championStats.AttackRange, championStats.MovementSpeed,
		championStats.AttackDamagePerLevel, championStats.ManaRegenPerLevel,
		championStats.CriticalDamagePerLevel, championStats.MagicResistPerLevel,
		championStats.CriticalDamage, championStats.ManaRegen, championStats.MagicResist,
		championStats.HealthRegen, championStats.HealthPointsPerLevel, championStats.Id)

	if err != nil {
		log.Println("ERROR UpdateChampionStats: ", err)
	}
}

func FindChampionById(connection *sql.DB, championId int64) model.Champions {
	query := "SELECT * FROM champions WHERE id = ?"

	resultSet := connection.QueryRow(query, championId)
	champion := model.Champions{}

	err := resultSet.Scan(&champion.Id,
		&champion.ChampionStatsId, &champion.Name,
		&champion.Title, &champion.ChampionPassiveId,
		&champion.ChampionSpellId, &champion.ChampionMasteryId)

	if err != nil {
		log.Println("ERROR FindChampionById: ", err)
		return model.Champions{}
	}

	return champion

}
