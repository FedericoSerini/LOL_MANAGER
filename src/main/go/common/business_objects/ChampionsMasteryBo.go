package business_objects

import (
	"../model"
	"database/sql"
	"log"
)

func PersistChampionMastery(connection *sql.DB, championMastery model.ChampionMastery) {
	query := "INSERT INTO champions_mastery (champion_level,chest_granted,champion_points,champion_points_since_last_level,champion_points_until_next_level,tokens_earned,last_play_time, champion_id, player_id) VALUES (?,?,?,?,?,?,?,?,?)"
	_, err := connection.Exec(query,
		championMastery.ChampionLevel,
		championMastery.ChestGranted,
		championMastery.ChampionPoints,
		championMastery.ChampionPointsSinceLastLevel,
		championMastery.ChampionPointsUntilNextLevel,
		championMastery.TokensEarned,
		championMastery.LastPlayTime,
		championMastery.ChampionId,
		championMastery.PlayerId)

	if err != nil {
		log.Println("ERROR PersistChampionMastery: ", err)
	}
}

func UpdateChampionMastery(connection *sql.DB, championMastery model.ChampionMastery) {

	query := "UPDATE champions_mastery SET champion_level=?,chest_granted=?,champion_points=?,champion_points_since_last_level=?,champion_points_until_next_level=?,tokens_earned=?,last_play_time=? WHERE champion_id=? AND player_id=?"
	_, err := connection.Exec(query,
		championMastery.ChampionLevel,
		championMastery.ChestGranted,
		championMastery.ChampionPoints,
		championMastery.ChampionPointsSinceLastLevel,
		championMastery.ChampionPointsUntilNextLevel,
		championMastery.TokensEarned,
		championMastery.LastPlayTime,
		championMastery.ChampionId,
		championMastery.PlayerId)

	if err != nil {
		log.Println("ERROR UpdateChampionMastery: ", err)
	}
}

func FindChampionMasteryByChampionId(connection *sql.DB, championId int32) model.ChampionMastery {
	query := "SELECT *  FROM champions_mastery WHERE champion_id = ?"
	resultSet := connection.QueryRow(query, championId)
	championMastery := model.ChampionMastery{}

	err := resultSet.Scan(&championMastery.Id, &championMastery.ChampionLevel, &championMastery.ChestGranted,
		&championMastery.ChampionPoints, &championMastery.ChampionPointsSinceLastLevel, &championMastery.PlayerId,
		&championMastery.ChampionPointsUntilNextLevel, &championMastery.TokensEarned, &championMastery.ChampionId, &championMastery.LastPlayTime)

	if err != nil {
		log.Println("ERROR FindChampionMasteryByChampionId: ", err)
		return model.ChampionMastery{}
	}

	return championMastery
}
