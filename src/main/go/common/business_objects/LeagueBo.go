package business_objects

import (
	"../model"
	"database/sql"
	"log"
)

func PersistLeague(connection *sql.DB, league model.League) {
	query := "INSERT INTO summoner_league (rank,queue_type,hot_streak,mini_series_id,wins,veteran,losses,fresh_blood,league_id,player_or_team_name,inactive,player_or_team_id,league_name,tier,league_points) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
	_, err := connection.Exec(query,
		league.Rank,
		league.QueueType,
		league.HotStreak,
		league.MiniSeries,
		league.Wins,
		league.Veteran,
		league.Losses,
		league.FreshBlood,
		league.LeagueId,
		league.PlayerOrTeamName,
		league.Inactive,
		league.PlayerOrTeamId,
		league.LeagueName,
		league.Tier,
		league.LeaguePoints)

	if err != nil {
		log.Println("ERROR PersistLeague: ", err)
	}
}

func UpdateLeague(connection *sql.DB, league model.League) {

	query := "UPDATE summoner_league SET rank=?,queue_type=?,hot_streak=?,mini_series_id=?,wins=?,veteran=?,losses=?,fresh_blood=?,league_id=?,inactive=?,league_name=?,tier=?,league_points=? WHERE player_or_team_id=?"
	_, err := connection.Exec(query,
		league.Rank,
		league.QueueType,
		league.HotStreak,
		league.MiniSeries,
		league.Wins,
		league.Veteran,
		league.Losses,
		league.FreshBlood,
		league.LeagueId,
		league.Inactive,
		league.LeagueName,
		league.Tier,
		league.LeaguePoints,
		league.PlayerOrTeamId)

	if err != nil {
		log.Println("ERROR UpdateLeague: ", err)
	}
}

func FindLeagueByPlayerId(connection *sql.DB, playerId string) model.League {
	query := "SELECT DISTINCT * FROM summoner_league WHERE  player_or_team_id = ?"
	resultSet := connection.QueryRow(query, playerId)
	league := model.League{}
	err := resultSet.Scan(&league.Id, &league.Rank, &league.QueueType, &league.HotStreak, &league.MiniSeries, &league.Wins, &league.Veteran,
		&league.Losses, &league.FreshBlood, &league.LeagueId, &league.PlayerOrTeamName, &league.Inactive, &league.PlayerOrTeamId,
		&league.LeagueName, &league.Tier, &league.LeaguePoints)

	if err != nil {
		log.Println("ERROR FindLeagueByPlayerId: ", err)
		return model.League{}
	}

	return league
}
