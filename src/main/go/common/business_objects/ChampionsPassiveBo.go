package business_objects

import (
	"../model"
	"database/sql"
	"log"
)

func PersistChampionPassive(connection *sql.DB, championPassive model.ChampionPassive) int64 {
	return 0
}

func UpdateChampionPassive(connection *sql.DB, championPassive model.ChampionPassive) {
	query := "UPDATE champions_passive SET name = ?, sanitized_description = ?, full_image = ?, group_image = ?, image_sprite = ?, image_height = ?, image_width = ?, image_y = ?, image_x = ?, description = ? WHERE id = ?"
	_, err := connection.Exec(query,
		championPassive.Name, championPassive.SanitizedDescription,
		championPassive.FullImage, championPassive.GroupImage,
		championPassive.ImageSprite, championPassive.ImageHeight, championPassive.ImageWidth,
		championPassive.ImageY, championPassive.ImageX,
		championPassive.Description, championPassive.Id)

	if err != nil {
		log.Println("ERROR UpdateChampionStats: ", err)
	}
}

func FindChampionPassiveByChampionId(connection *sql.DB, championId int64) model.ChampionPassive {
	query := "SELECT cp.* FROM champions as c,champions_passive cp WHERE c.id = ? and cp.id = c.passive_id"

	resultSet := connection.QueryRow(query, championId)
	passive := model.ChampionPassive{}

	err := resultSet.Scan(&passive.Id,
		&passive.Name, &passive.SanitizedDescription,
		&passive.FullImage, &passive.GroupImage,
		&passive.ImageSprite, &passive.ImageHeight,
		&passive.ImageWidth,&passive.ImageY,
		&passive.ImageX,&passive.Description)

	if err != nil {
		log.Println("ERROR FindChampionPassiveByChampionId: ", err)
		return model.ChampionPassive{}
	}

	return passive

}