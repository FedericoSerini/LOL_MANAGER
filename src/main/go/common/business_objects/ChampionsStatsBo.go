package business_objects

import (
	"../model"
	"database/sql"
	"log"
)

func PersistChampionStats(connection *sql.DB, championStats model.ChampionStats) int64 {
	query := "INSERT INTO champions_stats (armor_per_level, attack_damage, mana_points_per_level, attack_speed_offset, mana_points, armor, health_points, health_regen_per_level, attack_speed_per_level, attack_range, movement_speed, attack_damage_per_level, mana_regen_per_level, critical_damage_per_level, magic_resist_per_level, critical_damage, mana_regen, magic_resist, health_regen, health_points_per_level) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
	result, err := connection.Exec(query,
		championStats.ArmorPerLevel, championStats.AttackDamage,
		championStats.ManaPointsPerLevel, championStats.AttackSpeedOffset,
		championStats.ManaPoints, championStats.Armor, championStats.HealthPoints,
		championStats.HealthRegenPerLevel, championStats.AttackSpeedPerLevel,
		championStats.AttackRange, championStats.MovementSpeed,
		championStats.AttackDamagePerLevel, championStats.ManaRegenPerLevel,
		championStats.CriticalDamagePerLevel, championStats.MagicResistPerLevel,
		championStats.CriticalDamage, championStats.ManaRegen, championStats.MagicResist,
		championStats.HealthRegen, championStats.HealthPointsPerLevel)

	connection.Query(query)
	id, err2 := result.LastInsertId()

	if err != nil {
		log.Println("ERROR PersistChampionStats: ", err, "last insert id err: ", err2)
		return 0
	}

	return id
}

func UpdateChampionStats(connection *sql.DB, championStats model.ChampionStats) {
	query := "UPDATE champions_stats SET armor_per_level=?, attack_damage = ?, mana_points_per_level = ?, attack_speed_offset = ?, mana_points = ?, armor = ?, health_points = ?, health_regen_per_level = ?, attack_speed_per_level = ?, attack_range = ?, movement_speed = ?, attack_damage_per_level = ?, mana_regen_per_level = ?, critical_damage_per_level = ?, magic_resist_per_level = ?, critical_damage = ?, mana_regen = ?, magic_resist = ?, health_regen = ?, health_points_per_level = ? WHERE id = ?"
	_, err := connection.Exec(query,
		championStats.ArmorPerLevel, championStats.AttackDamage,
		championStats.ManaPointsPerLevel, championStats.AttackSpeedOffset,
		championStats.ManaPoints, championStats.Armor, championStats.HealthPoints,
		championStats.HealthRegenPerLevel, championStats.AttackSpeedPerLevel,
		championStats.AttackRange, championStats.MovementSpeed,
		championStats.AttackDamagePerLevel, championStats.ManaRegenPerLevel,
		championStats.CriticalDamagePerLevel, championStats.MagicResistPerLevel,
		championStats.CriticalDamage, championStats.ManaRegen, championStats.MagicResist,
		championStats.HealthRegen, championStats.HealthPointsPerLevel, championStats.Id)

	if err != nil {
		log.Println("ERROR UpdateChampionStats: ", err)
	}
}

func FindChampionStatByChampionId(connection *sql.DB, championId int64) model.ChampionStats {
	query := "SELECT cs.* FROM champions as c, champions_stats as cs WHERE cs.id = c.stat_id AND c.id = ?"
	resultSet := connection.QueryRow(query, championId)
	championStats := model.ChampionStats{}

	err := resultSet.Scan(&championStats.Id,
		&championStats.ArmorPerLevel, &championStats.AttackDamage,
		&championStats.ManaPointsPerLevel, &championStats.AttackSpeedOffset,
		&championStats.ManaPoints, &championStats.Armor, &championStats.HealthPoints,
		&championStats.HealthRegenPerLevel, &championStats.AttackSpeedPerLevel,
		&championStats.AttackRange, &championStats.MovementSpeed,
		&championStats.AttackDamagePerLevel, &championStats.ManaRegenPerLevel,
		&championStats.CriticalDamagePerLevel, &championStats.MagicResistPerLevel,
		&championStats.CriticalDamage, &championStats.ManaRegen, &championStats.MagicResist,
		&championStats.HealthRegen, &championStats.HealthPointsPerLevel)

	if err != nil {
		log.Println("ERROR FindChampionStatByChampionId: ", err)
		return model.ChampionStats{}
	}

	return championStats
}
