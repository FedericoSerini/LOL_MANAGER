package business_objects

import (
	"../model"
	"database/sql"
	"log"
)

func PersistChampionSpell(connection *sql.DB, championSpells model.ChampionSpells) int64 {

	query := "INSERT INTO champions_spells (spell_name, spell_cooldown, effect, cost, cooldown, spell_range, cost_type, sanitized_tooltip, resource, effect_level_tip, label, ranks_with, dyn, link, coeff, key_vars, full_image, group_image, image_sprite, image_height, image_width, image_y, image_x, sanitized_description, tool_tip, max_rank, cost_burn, range_burn, key, description, effect_burn) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
	result, err := connection.Exec(query,
		championSpells.SpellName, championSpells.SpellCooldown, championSpells.Effect,
		championSpells.Cost, championSpells.Cooldown, championSpells.SpellRange,
		championSpells.CostType, championSpells.SanitizedTooltip, championSpells.Resource,
		championSpells.EffectLevelTip, championSpells.Label, championSpells.RanksWith,
		championSpells.Dyn, championSpells.Link, championSpells.Coeff, championSpells.KeyVars,
		championSpells.FullImage, championSpells.GroupImage, championSpells.ImageSprite,
		championSpells.ImageHeight, championSpells.ImageWidth, championSpells.ImageY,
		championSpells.ImageX, championSpells.SanitizedDescription, championSpells.Tooltip,
		championSpells.MaxRank, championSpells.CostBurn, championSpells.RangeBurn,
		championSpells.Key, championSpells.Description, championSpells.EffectBurn)

	connection.Query(query)
	id, err2 := result.LastInsertId()

	if err != nil {
		log.Println("ERROR PersistChampionSpell: ", err, "last insert id err: ", err2)
		return 0
	}

	return id
}

func UpdateChampionSpell(connection *sql.DB, championSpell model.ChampionSpells) {
	query := "UPDATE champions_spells SET spell_name=?, spell_cooldown = ?, effect = ?, cost = ?, cooldown = ?, spell_range = ?, cost_type = ?, sanitized_tooltip = ?, resource = ?, effect_level_tip = ?, label = ?, dyn = ?, link = ?, critical_damage_per_level = ?, magic_resist_per_level = ?, critical_damage = ?, mana_regen = ?, magic_resist = ?, health_regen = ?, health_points_per_level = ? WHERE id = ?"
	_, err := connection.Exec(query,
		&championSpell.SpellName, &championSpell.SpellCooldown, &championSpell.Effect,
		&championSpell.Cost, &championSpell.Cooldown, &championSpell.SpellRange, &championSpell.CostType,
		&championSpell.SanitizedTooltip, &championSpell.Resource, &championSpell.EffectLevelTip, &championSpell.Label,
		&championSpell.RanksWith, &championSpell.Dyn, &championSpell.Link, &championSpell.Coeff, &championSpell.KeyVars,
		&championSpell.FullImage, &championSpell.GroupImage, &championSpell.ImageSprite, &championSpell.ImageHeight,
		&championSpell.ImageWidth, &championSpell.ImageY, &championSpell.ImageX, &championSpell.SanitizedDescription,
		&championSpell.Tooltip, &championSpell.MaxRank, &championSpell.CostBurn, &championSpell.RangeBurn,
		&championSpell.Key, &championSpell.Description, &championSpell.EffectBurn, championSpell.Id)

	if err != nil {
		log.Println("ERROR UpdateChampionStats: ", err)
	}
}

func FindChampionSpellsByChampionId(connection *sql.DB, championId int64) model.ChampionSpells {
	query := "SELECT cs.* FROM champions as c, champions_spells as cs WHERE cs.id = c.spell_id AND c.id = ?"
	resultSet, err := connection.Query(query, championId)

	championSpell := model.ChampionSpells{}




	resultSet.Scan(&championSpell.Id, &championSpell.SpellName, &championSpell.SpellCooldown, &championSpell.Effect,
		&championSpell.Cost, &championSpell.Cooldown, &championSpell.SpellRange, &championSpell.CostType,
		&championSpell.SanitizedTooltip, &championSpell.Resource, &championSpell.EffectLevelTip, &championSpell.Label,
		&championSpell.RanksWith, &championSpell.Dyn, &championSpell.Link, &championSpell.Coeff, &championSpell.KeyVars,
		&championSpell.FullImage, &championSpell.GroupImage, &championSpell.ImageSprite, &championSpell.ImageHeight,
		&championSpell.ImageWidth, &championSpell.ImageY, &championSpell.ImageX, &championSpell.SanitizedDescription,
		&championSpell.Tooltip, &championSpell.MaxRank, &championSpell.CostBurn, &championSpell.RangeBurn,
		&championSpell.Key, &championSpell.Description, &championSpell.EffectBurn)

	if err != nil {
		log.Println("ERROR FindChampionStatByChampionId: ", err)
		return model.ChampionSpells{}
	}

	return championSpell
}
