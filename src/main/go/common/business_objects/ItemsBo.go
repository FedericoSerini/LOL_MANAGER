package business_objects

import (
	"../model"
	"database/sql"
	"log"
)

func PersistItem(connection *sql.DB, item model.Items) {
	query := "INSERT INTO items (id,name,description,sanitized_description,base_gold_price,sell_gold_price,total_full_item_gold) VALUES (?,?,?,?,?,?,?)"
	_, err := connection.Exec(query,
		item.ID, // PK
		item.Name,
		"-- POINTLESS DESCRIPTION --",
		item.SanitizedDescription,
		item.Base,
		item.Sell,
		item.Total)

	if err != nil {
		log.Println("ERROR PersistItem: ", err)
	}
}
