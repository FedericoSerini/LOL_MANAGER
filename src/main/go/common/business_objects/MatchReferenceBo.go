package business_objects

import (
	"../model"
	"database/sql"
	"log"
)

func PersistMatchReference(connection *sql.DB, matchReference model.MatchReference) {
	query := "INSERT INTO match_reference (id,lane,champion_id,platform_id,match_timestamp,queue,role,season) VALUES (?,?,?,?,?,?,?,?)"
	_, err := connection.Exec(query,
		matchReference.GameId,
		matchReference.Lane,
		matchReference.Champion,
		matchReference.PlatformId,
		matchReference.Timestamp,
		matchReference.Queue,
		matchReference.Role,
		matchReference.Season)

	if err != nil {
		log.Println("ERROR PersistMatchReference: ", err)
	}
}
