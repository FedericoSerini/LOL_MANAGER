package config

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"log"
)

func GetDatabaseConnection() *sql.DB {
	DatabaseConnection, err := sql.Open("sqlite3",
		"src/main/go/resources/database/lol_manager.db")

	if err != nil || DatabaseConnection.Ping() != nil {
		log.Println("ERROR: DATABASE_INIT: ", err)
	} else {
		log.Println("DATABASE CONNECTION SUCCESSFULLY OPENED")
	}

	return DatabaseConnection
}
